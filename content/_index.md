## How this blog came about and what (not) to expect from it
I'm a PhD student in systems biology and during my thesis write-up I have found
myself needing a less formal outlet for some of my thoughts. This blog supplies
that outlet. The content is not meticulously thought-through, formatted or even
spell-checked but rather a collection of ideas that I have jotted down.

