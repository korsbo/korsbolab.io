---
title: About me
subtitle: Niklas Korsbo
comments: false
---

I'm a PhD student at Cambridge University where I study singal processing and
regulatory mechanisms which are important for the proper functioning of
biological organisms. 
I come from a background of theoretical physics and I'm currently affiliated with both the Department for Applied Mathematics and Theoretical Physics as well as the plant science lab The Sainsbury Laboratory. 

