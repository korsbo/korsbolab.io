using Latexify
using Plots
using DifferentialEquations
using DiffEqBiological: @reaction_network
using ParameterizedFunctions
Plots.default(grid=false,  guidefontsize=16, tickfontsize=12, legendfontsize=12, size=(400,300))
solve = DifferentialEquations.solve

function latexprint(args...; kwargs...) 
    str = latexify(args...; mathjax=true, starred=false, kwargs...).s
    str = replace(str, "_"=>raw"\_")
    str = replace(str, "\\\\"=>raw"\newline")
    display("text/latex", str)
end

function simulate(model, u0, p; tspan=(0., 20.), tstep=2, ystep=2)
    cond(u, t, i) = t - tstep
    affect!(i) = (i.p[1] = ystep)
    cb = ContinuousCallback(cond, affect!)
    prob = ODEProblem(nf, u0, tspan, p)
    sol = solve(prob; callback=cb)
end

function equilibrate(model, u0, p)
    prob = ODEProblem(nf, u0, (0., 1e3), p)
    sol = solve(prob)
    sol.u[end]
end

