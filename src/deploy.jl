using Weave
root = split(@__DIR__(), "src")[1]
postdir = joinpath(root, "content", "post")
srcdir = joinpath(root, "content", "src")


function deploy(fpath::String; kwargs...)
    fname = splitdir(fpath)[end]
    outputfile = joinpath(postdir, splitext(fname)[1] * ".md")
    rm(outputfile; force=true)
    weave(
          fpath;
          out_path=postdir,
          doctype="pandoc",
          kwargs...
         )
    outputfile = joinpath(postdir, splitext(fname)[1] * ".md")
    run(`sed -i 's/\!\[\](figures/![](\/post\/figures/' $outputfile`)
    run(`sed -i 's/\~\{13\}/~~~~/' $outputfile`)
    # run(`printf "%s\n" '1,$s/figures/postfigures/' wq | ed -s $outputfile`)
end

